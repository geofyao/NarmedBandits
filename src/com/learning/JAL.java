package com.learning;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jey on 10/12/2016.
 */
public class JAL {
    List<Player> players;
    List<List<Action>> actions;
    JAL(){
        players= new ArrayList<>();
        players.add(new Player(true));
        players.add(new Player(false));
    }

    public void play(){
        for(int z = 0; z<Main.experiences; z++) {
            for (int k = 0; k < Main.steps; k++) {
                int[] selectedActions=new int[]{-1,-1};
                for (int v = 0; v < players.size(); v++) {
                    //region TYPE 1 expected values computation
                    double[] ev= {0.,0.,0.};
                    for(int i=0;i<ev.length;i++){
                        for(int j=0;j<3;j++){
                            if(players.get(v).isRowPlayer()){
                                double pr= (double)players.get((v+1)%players.size()).getCount(i)/(k+1);
                                ev[i] += actions.get(i).get(j).getQValue()*pr;
                            }else{
                                double pr= (double)actions.get(j).get(i).getTotalSelection()/(k+1);
                                ev[i] += actions.get(j).get(i).getQValue()*pr;
                            }
                        }
                    }
                    //endregion
                    selectedActions[v]=players.get(v).selectAction(ev);
                }
                //les actions ont été selectionnées
                double rewards=actions.get(selectedActions[0]).get(selectedActions[1]).computeReward(Main.random.nextGaussian());
                Main.averageRewards.set(k,rewards);
            }
        }
    }

    private void resetAction(){
        actions= new ArrayList<>();
        double stDev=   0.2;
        double stDev0=  0.2;
        double stDev1=  0.2;

        List<Action> tmp1= new ArrayList<>();
        List<Action> tmp2= new ArrayList<>();
        List<Action> tmp3= new ArrayList<>();
        tmp1.add(new Action(11,Math.pow(stDev0,2))); tmp1.add(new Action(-30,Math.pow(stDev,2))); tmp1.add(new Action(0,Math.pow(stDev,2)));
        tmp2.add(new Action(-30,Math.pow(stDev,2))); tmp2.add(new Action(7,Math.pow(stDev1,2)));  tmp2.add(new Action(6,Math.pow(stDev,2)));
        tmp3.add(new Action(0,Math.pow(stDev,2)));   tmp3.add(new Action(0,Math.pow(stDev,2)));   tmp3.add(new Action(5,Math.pow(stDev,2)));
        actions.add(tmp1);
        actions.add(tmp2);
        actions.add(tmp3);
    }
}
