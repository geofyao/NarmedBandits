package com.learning;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jey on 10/12/2016.
 */
public class Player {

    List<Action> actions;
    private boolean rowPlayer;
    private int[] selectionCount;
    Player(boolean rowPlayer){
        actions= new ArrayList<>();
        this.rowPlayer=rowPlayer;
        selectionCount= new int[]{0, 0, 0};
    }

    public void setRewardAndUpdate(double reward){

    }

    public int selectAction(double[] ev){
        int selectedAction=-1;
        double[] probabilities= computeProbabilities(ev);
        double chances= Main.random.nextDouble();
        double sum=0;
        int id=0;
        while(selectedAction==-1 && id<=3){
            sum+=probabilities[id];
            if(chances<=sum){
                selectedAction=id;
            }
            id++;

        }
        selectionCount[selectedAction]+=1;
        return selectedAction;
    }


    private double[] computeProbabilities(double[] ev){
        double tau=0.1;
        double[] probabilities= new double[3];
        double den=0.;
        //calcul du numérateur
        for(int i=0;i<probabilities.length;i++){
            probabilities[i]=Math.exp(ev[i]/tau);
            den+=probabilities[i];
        }
        //calcul de la proba de chaque action
        for(int i=0;i<probabilities.length;i++){
            probabilities[i]/=den;
        }

        return probabilities;
    }

    public boolean isRowPlayer(){
        return rowPlayer;
    }

    public int getCount( int action){
        return selectionCount[action];
    }

}
