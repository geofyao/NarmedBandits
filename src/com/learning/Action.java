package com.learning;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jey on 26/11/2016.
 */
public class Action {
    private  double totalReward;
    private int totalSelection;
    private double mean;
    private double stDev;
    private double reward;
    private double q =5.;
    private List<Double> qValues;

    Action(){
        this.totalReward=0;
        this.totalSelection=0;
        qValues =new ArrayList<>();

    }

    Action(double mean,double stDev){
        this();
        this.mean=mean;
        this.stDev=stDev;
    }

    public Double computeReward(double gaussianVal){
        reward =(gaussianVal*stDev)+mean;
        return reward;
    }

    public Double getReward(){
        return reward;
    }

    public double getQValue(){
        return q;
    }

    public void update(){
        totalSelection++;
        q=q+((reward -q)/totalSelection);
        qValues.add(q);
    }

    public List<Double> getqValues(){
        return qValues;
    }

    public int getTotalSelection(){
        return  totalSelection;
    }
}
