package com.learning;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    static Random random;
    static List<Double> averageRewards;
    static int experiences =1;
    static int steps=5000;
    static List<Double> zeroes;
    public static void main(String[] args) {
        random= new Random(Instant.now().toEpochMilli());
        averageRewards =new ArrayList<>();
        zeroes=new ArrayList<>();
        for(int i=0;i<steps;i++){
            zeroes.add(0.);
        }
        exo1();
    }

    /**
     * Egreedy algorithm, if we want to use the greedy algorithm then epsilon must be 0
     */
    public static void eGreedy(){
        int selectedId;
        List<List<Double>> qValues= new ArrayList<>();
        List<Integer> selection= new ArrayList<>();
        for(int i=0;i<4;i++){
            qValues.add(new ArrayList<>(zeroes));
            selection.add(0);

        }
        for(int z = 0; z< experiences; z++){
             List<Action> actions= resetActions();
            for(int j=0;j<steps;j++){
                double epsilon=1/Math.sqrt(j);
                double probability=random.nextDouble();
                selectedId=random.nextInt(actions.size());
                if(probability>epsilon){//we use the max
                    //on va déterminer quelle est l'id de la meilleure action
                    for(int i=0;i<actions.size();i++){
                        //values.add(actions.get(i).computeValue(random.nextGaussian()));
                        if(actions.get(selectedId).getQValue()<actions.get(i).getQValue()){
                            selectedId=i;
                        }
                    }
                }

                //ici on a déterminé l'action qu'on va utliser
                double reward= actions.get(selectedId).computeReward(random.nextGaussian());
                actions.get(selectedId).update();
                averageRewards.set(j,averageRewards.get(j)+reward);
                for(int v=0;v<4;v++){
                    qValues.get(v).set(j,qValues.get(v).get(j)+actions.get(v).getQValue());
                }
            }
            for(int i=0;i<actions.size();i++){
                selection.set(i,actions.get(i).getTotalSelection()+selection.get(i));
            }

        }
        String color="blue";
       /* if(epsilon==0.2) {
            color = "red";
        }else if(epsilon==0.1){
            color="yellow";
        }*/
        String ret="\\addplot[color="+color+",mark=none]\n coordinates {\n " ;
        System.out.println("%greedy average rewards epsilon: 1/sqrt(t)");
        getAverageResult(averageRewards,ret,"\\epsilon=1/\\sqrt{t}");
        for(int v=0;v<qValues.size();v++) {
            System.out.println("%Qvalues for action " + (v+1));
            getAverageResult(qValues.get(v), ret,"\\epsilon=1/\\sqrt{t}");
        }
        System.out.println("%greedy average histogram selection epsilon: 1/sqrt(t)");
        getAverageHisto(selection,"\\epsilon=1/\\sqrt{t}");

    }

    public static void softMax(){
        int selectedId=-1;
        List<List<Double>> qValues= new ArrayList<>();
        List<Integer> selection= new ArrayList<>();
        for(int i=0;i<4;i++){
            qValues.add(new ArrayList<>(zeroes));
            selection.add(0);

        }
        for(int z = 0; z< experiences; z++){
            List<Action> actions= resetActions();
            for(int j=0;j<steps;j++){
                double var=(double)(1000-j)/1000;
                double tau=4*var;
                double[] probabilities= new double[actions.size()];
                double chances= random.nextDouble();
                double den=0.;
                selectedId=-1;
                //calcul du numérateur
                for(int i=0;i<probabilities.length;i++){
                    probabilities[i]=Math.exp(actions.get(i).getQValue()/tau);
                    den+=probabilities[i];
                }
                //calcul de la proba de chaque action
                for(int i=0;i<probabilities.length;i++){
                    probabilities[i]/=den;
                }
                double sum=0;
                int id=0;
                while(selectedId==-1 && id<=3){
                    sum+=probabilities[id];
                    if(chances<=sum){
                        selectedId=id;
                    }
                    id++;

                }
                //ici on a déterminé l'action qu'on va utliser
                double reward= actions.get(selectedId).computeReward(random.nextGaussian());
                actions.get(selectedId).update();
                averageRewards.set(j,averageRewards.get(j)+reward);
                for(int v=0;v<4;v++){
                    qValues.get(v).set(j,qValues.get(v).get(j)+actions.get(v).getQValue());
                }
            }
            for(int i=0;i<actions.size();i++){
                selection.set(i,actions.get(i).getTotalSelection()+selection.get(i));
            }
        }

        System.out.println("%softmax average rewards tau: 4*(1000-t)/1000");
        /*String color="orange";
        if(tau==0.1)
            color="olive";
        String ret="\\addplot[color="+color+",mark=none]\n coordinates {\n " ;
        getAverageResult(averageRewards,ret,"\\tau="+tau);
        for(int v=0;v<qValues.size();v++) {
            System.out.println("%Qvalues for action " + (v+1));
            getAverageResult(qValues.get(v), ret, "\\tau=" + tau);
        }
        System.out.println("%greedy average histogram selection epsilon: "+tau);
        getAverageHisto(selection,"\\tau="+tau);*/
        String ret="\\addplot[color=dandelion,mark=none]\n coordinates {\n " ;
        getAverageResult(averageRewards,ret,"\\tau=4*\\dfrac{1000-t}{1000}");
        for(int v=0;v<qValues.size();v++) {
            System.out.println("%Qvalues for action " + (v+1));
            getAverageResult(qValues.get(v), ret, "\\tau=4*\\dfrac{1000-t}{1000}" );
        }
        System.out.println("%greedy average histogram selection tau: 4*1000t-/1000");
        getAverageHisto(selection,"\\tau=4*\\dfrac{1000-t}{1000}");



    }

    public static void randomChoice(){
        List<List<Double>> qValues= new ArrayList<>();
        List<Integer> selection= new ArrayList<>();
        for(int i=0;i<4;i++){
            qValues.add(new ArrayList<>(zeroes));
            selection.add(0);

        }
        for(int z = 0; z< experiences; z++){
            List<Action> actions= resetActions();
            for(int i=0;i<steps;i++){
                int selectedId= random.nextInt(actions.size());
                double reward= actions.get(selectedId).computeReward(random.nextGaussian());
                actions.get(selectedId).update();
                averageRewards.set(i,averageRewards.get(i)+reward);
                for(int v=0;v<4;v++){
                    qValues.get(v).set(i,qValues.get(v).get(i)+actions.get(v).getQValue());
                }
            }
            for(int i=0;i<actions.size();i++){
                selection.set(i,actions.get(i).getTotalSelection()+selection.get(i));
            }
        }
        System.out.println("%Random average rewards");
        String ret="\\addplot[mark=none]\n coordinates {\n " ;
        getAverageResult(averageRewards,ret ,"random");
        for(int v=0;v<qValues.size();v++) {
            System.out.println("%Qvalues for action " + (v+1));
            getAverageResult(qValues.get(v), ret, "random");
        }
        System.out.println("%greedy average histogram selection random ");
        getAverageHisto(selection,"random");

    }

    private static void getAverageHisto(List<Integer>selections, String legend){
        String ret="\\addplot \n" +
                "coordinates{";
        for(int i=0;i< selections.size();i++){
            ret+="("+(i+1)+","+selections.get(i)/ experiences +")";
        }
        ret+="};\n \\legend{"+legend+"}";
        System.out.println(ret);
    }

    private static void getAverageResult(List<Double>rewards, String param, String legend){
        String ret=param ;
        for (int i=0;i<rewards.size();i++) {
            ret+="("+i+","+rewards.get(i)/ experiences +")";
        }

        ret+="\n};";
        ret+="\n \\addlegendentry{$"+legend+"$}";
        System.out.println(ret);
    }

    private static void reinitalizeCounter(){
        averageRewards.clear();
        for(int i=0;i<steps;i++){
            averageRewards.add(0.);
        }
        resetActions();
    }

    private static List<Action> resetActions(){
        List<Action>actions= new ArrayList<>();
        actions.add(new Action(2.3,(0.9*2)));
        actions.add(new Action(2.1,(0.6*2)));
        actions.add(new Action(1.5,(0.4*2)));
        actions.add(new Action(1.3,(2*2)));
        return actions;
    }

    private static void exo1(){
        /* reinitalizeCounter();
        randomChoice();
        reinitalizeCounter();
        eGreedy(0);
        reinitalizeCounter();
        eGreedy(0.1);
        reinitalizeCounter();
        eGreedy(0.2);
        reinitalizeCounter();
        softMax(1);
        reinitalizeCounter();
        softMax(0.1);*/

        //reinitalizeCounter();
        //eGreedy();
        reinitalizeCounter();
        softMax();
    }

    private static void exo2(){
        JAL jal= new JAL();
        jal.play();
    }


}
